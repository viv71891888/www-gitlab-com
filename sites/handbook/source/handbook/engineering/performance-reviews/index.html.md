---
layout: handbook-page-toc
title: "Engineering Performance Reviews"
---

## Overview

Starting in FY21-Q4 (Calendar year 2020) GitLab is launching [a change to our performance and compensation review process](/handbook/people-group/performance-assessments-and-succession-planning/). The major changes are:

* The use of a [Performance/Potential matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) as the primary artifact.
* The deprecation of compa groups and the addition of [performance](/handbook/people-group/performance-assessments-and-succession-planning/#the-performance-factor) as an input to the compensation review process.

Engineering is going to deviate somewhat from this process. Assume everything on the [People Group's page](/handbook/people-group/performance-assessments-and-succession-planning/) is true, except where specifically overridden here.

## Why is Engineering doing something different?

* This change is coming fairly late in the annual review process, which doesn't give much time for Engineering team members to understand the new process.
* This change is another in a series of changes to prior annual reviews, which doesn't give Engineering team members time to demonstrate multi-year progress against the criteria.
* The "potential" axis of the Performance/Potential matrix will be hidden from team members this review, but potentially shared in the next review. Eventual transparency is good, but not as ideal as immediate transparency.
* A Performance/Potential matrix is a result, but it's more important to emphasize how you get that result when you're communicating it to the team member and fostering a discussion.

## What is Engineering doing?

Most of the people group's process will be preserved, and all of the external interfaces of the process (such as the subsequent compensation review) will be met. These are the differences:

* A **smaller iteration** in the process so
    * The lateness of the change is felt less
    * It will have more continuity with last year's review process
* We'll use a **templated document** instead of the 9-box artifact, with standardized inputs into the template to emphasise the process of determining the performance factor
    * This template was based on a FY21-Q3 performance review cycle that our Security department did successfully
* We **won't do the withheld "potential" axis**, so:
    * We can be **immediately transparent** with team members about all aspects of the process
    * We can **avoid the potentially negative experience** of taking a static opinion about someone's future, about which we choose to remain optimistic and not limit
* We will still **produce a company-standard [performance factor](/handbook/people-group/performance-assessments-and-succession-planning/#the-performance-factor)** as input into the compensation review (one of: "developing", "performing", or "exceeding")

Engineering likes to be experimental, so here is what we are doing on a department-by-department basis:

| Organization              | Head          | Standard Company Process | Process on this Page | Comments |
|---------------------------|---------------|--------------------------|----------------------|----------|
| Engineering Division      | Eric J        |                          | ✓                    | This applies to Eric's direct reports |
| Development Department    | Christopher L |                          | ✓                    | A additional manager calibration experiment using [this template](https://docs.google.com/spreadsheets/d/1_MA8iVS1sypRks4wWYjTfs7jOoCYYLA0vU56p-Vi1oY/edit) |
| Infrastructure Department | Steve L       | ✓                        |                      | As a smaller dept with 5 core teams they expect to be able to adjust quick enough to use the new process and provide useful feedback to aid Engineering in the future. Most Infrastructure leadership is also familiar with 9-box from previous experience. May choose not to use the new 9-box tool (gdocs instead) |
| Quality Department        | Mek S         |                          | ✓                    | Small dept with 5 teams. [Career matrix MVC](https://docs.google.com/spreadsheets/d/1DNJdI-LXSsEFQvLSBhIrU273sH9LiDftK6CBdGd80mE/edit#gid=0) completed and will be used in addition to determind performance factors |
| Security Department       | Johnathan H   |                          | ✓                    | The Security Department did the initial iteration upon which this process is based. So they just need to to determine performance factors with their team members and comminicate them to the People Group |
| Support Department        | Tom C         |                          | ✓                    | Support is developing a set of [resources](https://gitlab.com/groups/gitlab-com/support/-/epics/100) to help their managers determine performance factors |
| UX Department             | Christie L    |                          | ✓                    | Iteration on the previous process that removes compa groups but keeps existing performance/responsibiity guidelines in place. [Detailed plan of record](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9323) |

## Process

This is the process that managers in the Engineering Division should expect to follow, except for differences called out in the table above.

### Flow chart

Here is a conceptual diagram of the process.

```mermaid
graph TD;
  360 -. If team member and manager agree .-> tmp;
  job --> tmp;
  mtx -. If asset already exists .-> tmp;
  tmp --> pfx;
  tmp --> rev;
  rev --> tmp;

  360[("360 Review<br>Values Assessment")];
  job[("Job Family")];
  mtx[("Career Matrix")];
  tmp{"Engineering Performance<br>Review Template"};
  pfx[("Performance Factor Value<br>for Comp Review")];
  rev("Performance Review 1:1<br>betweenTeam Member and Direct Manager");

  click tmp "https://docs.google.com/document/d/1Qy6Uh6wL4twayv1642iLhD6UlePvRqPL8WJmYBXsAWM/edit"
```

### Steps for reviews FY21: 

1. 2020-10 till 2020-11: Managers should attend an upcoming training session on Performance Factor
    * Development, Quality, Security, Support and UX Department Managers should keep in mind they will be using a different artifact
    * Infrastructure Department managers should use the entire process as communicated in the training
1. 2020-10: The manager clones the [Engineering Performance Review template](https://docs.google.com/document/d/1Qy6Uh6wL4twayv1642iLhD6UlePvRqPL8WJmYBXsAWM/edit)
1. 2020-11: The manager begins to fill out the template using:
    * The values assessment from the 360 review, if the team member agrees (since expectations were set at the time that the 360 reviews would not impact compensation)
    * The responsibilities and requirements on the job family page
    * The attributes on the career matrix (if that department has created it for the role)
    * Their own understanding of the team member's performance
1. 2020-11: The manager will share the document with the team member and schedule a 1:1 to discuss performance. Ideally, the performance review document is completed prior to the discussion, the minimum requirement would be to have a strong draft to guide the discussion. The primary objective for this step is to obtain feedback and data points from team members on their performance to consider during the assessment process.
1. 2020-11: The team member and the manager will discuss the team member's performance (guided by the performance review document). This is the team members' chance to influence the manager's decision on the performance factor based on missing information, or interpretation of existing information
    * The actual performance factor value should not be discussed here as it needs to wait until approval from management.
1. 2020-11-24: The manager submits the performance factor to their Senior Leader via a spreadsheet for their Department. The template spreadsheets will be provided by the People Business Partner to the Department Heads.
1. 2020-11-25: The Department Head collects all performance factors in a spreadsheet from their teams, to be submitted to their People Business Partner, which will be uploaded to BambooHR for the compensation review.
1. 2020-12-15 till 2021-01-31: The manager can communicate the final performance rating to the team member over a performance conversation leveraging the completed [performance review document](https://docs.google.com/document/d/1Qy6Uh6wL4twayv1642iLhD6UlePvRqPL8WJmYBXsAWM/edit#heading=h.j3xr5kqp6s24). The template will be used as a backbone for this conversation and will be added to BambooHR by the manager after the discussion. 
