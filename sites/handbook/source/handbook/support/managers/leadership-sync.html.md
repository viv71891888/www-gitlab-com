---
layout: handbook-page-toc
title: Support Leadership Sync
description: "Weekly Support Leadership Sync meetings"
---

### Purpose

Support leadership (Managers, Senior Managers and Director) participate in cross-regional meetings weekly.

The purpose of these meetings is to:
- build relationships, trust and context between managers
- address operational items such as availability
- explain context for issues and explore creative ideas in real time
- refine the problem statement of each raised issue by clarifying intent

The purpose of these meetings is **not** to:
- figure out the priority of issues
- gather feedback that can be given asynchronously on related issues
- refine solutions or merge requests related to issues
- make decisions

**Note:** It is worth reiterating that these meetings are not for making decisions or discussing things that could just as effectively be discussed within issues.

### Organization of Support Leadership Meetings

#### Schedule

1. As a global team, we have to strike the right balance in using our synchronous time wisely, and be mindful of the time zones and people's availability. To enable this, the weekly leadership syncs rotate through one region every week.
1. The meetings are available in the GitLab Support calendar.
1. Every meeting is set to automatically record and the recording can be accessed via the [GitLab Videos Recorded](https://drive.google.com/drive/u/0/folders/0APOeuCQrsm4KUk9PVA) shared drive.

#### Agenda

1. The agenda and notes are in a Google Doc: [Support leadership calls agenda and notes](https://drive.google.com/drive/u/0/search?q=parent:1eBkN9gosfqNVSoRR9LkS2MHzVGjM5-t5%20%22Support%20leadership%20calls%20agenda%20and%20notes%20(2021)%22) (internal only)
1. The agenda outline for next week should be created as the last item in the current week's sync. All agenda items should be added before the sync meeting of the week. Anything that is added later will most likely be ignored by most or all of the leadership, or not discussed, losing value. Appropriate Slack channels must be used to discuss any emergency items, especially if the sync meeting of the week is already done.
1. Please follow the below guidelines when adding agenda items:
   1. **Operational**: This section is for standing weekly updates regarding Hiring, Availability, Customer Accounts, Team Call-Outs etc.
   1. **Discuss**: Items under this section should be for things that need to be synchronously discussed, or need input. These may or may not have an associated issue.
   1. **Inform**: Items under this section are:
      1. issues we want to bring to everybody's attention in order to request **asynchronous** input
      1. things we should all be aware of, such as a new process around expense approval, updates from People Business Partner, etc.
   1. **Confidential Doc**: Use this document for discussion surrounding a specific individual's promotion, performance or individual situation, discussion about sensitive or confidential issues and processes. Add `See confidential doc item` as an item under `Inform` or `Discuss` in the general agenda depending on the action required for the item you've added in the confidential doc.
1. Each meeting has a chairperson to ensure that voices are heard equally and we make progress through the agenda.
1. All participants should be familiar with the [Video Calls Section](/handbook/communication/#video-calls) of the Communication page in the Handbook.
1. See this video, [Managing Support Leadership Sync Agendas and Meetings](https://drive.google.com/drive/u/0/search?q=managing%20support%20leadership%20sync%20agendas%20and%20meetings%20parent:1h1iaTgBbZJZG7CBNYUCRQPhMXe1xXlbM), for more information, including how to edit and use the agenda and notes document. 

### Call Procedure
#### As a chair
1. Review the meeting agenda before the meeting time.
1. Review the `Operational` section of the agenda and mention anything that may have an action item, or may need an action item such as:
   1. *Hiring data*: _Is there anything that is of note for the regions that are meeting?_
   1. *Holidays*: _Do we have (or need) a coverage plan for this holiday?_
   1. *Availability*: _How are the different regions looking in terms of availability next week?_
   1. *Metrics Review*: _Is there anything that needs to be highlighted to the executive team?_ 
   1. *Group Conversation*: _Please take a look at the slides, and add any points of interest for the general company_

1. Facilitate discussion of the items in the `Discuss` section of the agenda document.
1. Be aware when people unmute - this is an indication that they have something to say. If necessary, please interrupt and pass the floor. We want everyone to contribute, so it's your job to make sure that this can happen.
1. Assign any action items - including summarizing the discussion in an issue.
1. Stop the recording before discussing items in the confidential doc, if there are any.
1. Create the agenda outline for next week from the template as the last step.

#### As a manager
1. Add agenda items no later than before the start of that week's sync meeting.
1. Review each agenda item. If you have any feedback for items under `Discuss` and will not be joining the meeting synchronously, add them under `Async Notes` for that item.

#### As a synchronous meeting participant
1. If you raised an item for discussion, be brief (everyone should be familiar with the discussion), clear (why did you bring this item to this meeting), and understanding (others may interrupt you while speaking due to the nature of video calls (For more on this see point 13 in the [Video Calls Section](/handbook/communication/#video-calls) of the Communication page)).
1. Use majority of the meeting time to build rapport with the folks on the call.
   1. Talk through team related call-outs, share tips and tricks, latest learnings and others. 
   1. You should feel empowered to talk about things not on the agenda - sometimes, we need our peers' guidance and experience on things that might be bothering or challenging us.
1. After the meeting, note any points that you made during the meeting that were relevant to the discussion.

#### As an asynchronous meeting participant
1. Set aside time at the end of the week to review the agenda document and/or meeting recording from that week.
1. Make note of the things under `Inform` and `Discuss`, and contribute via respective issues.
