---
layout: handbook-page-toc
title: "Effective Discovery"
description: "Effective discovery is critical to thoroughly understanding customer needs and establishes the foundation for being able to effectively articulate GitLab's value and differentiation in a compelling, customer-centric way"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# Overview 
Opportunities can be won or lost during the discovery process. Effective discovery is critical to thoroughly understanding customer needs and establishes the foundation for being able to effectively articulate GitLab's value and differentiation in a compelling, customer-centric way. Effective discovery will help you attach to your customers' biggest problem(s), gain access to higher-level business stakeholders, and serve as a trusted advisor to your customers. This, in turn, will help you build and maintain a healthy and predictable pipeline to help you meet and exceed your sales goals.
- [Executing Great Discovery](https://podcasts.apple.com/us/podcast/33-executing-great-discovery-w-brian-walsh/id991362894?i=1000493740591) (podcast, 31 minutes)

# Qualification
The first phase of any discovery process is to determine whether or not you should spend time on a particular opportunity pursuit.

A good guide for this phase is codified in the [Criteria for Sales Accepted Opportunity (SAO)](/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao) and includes confirmation of 
- Authority
- Initiative
- Fit, and 
- Timing 

Ideally, effective qualification also includes identification of the prospect's 
- Cloud strategy and/or preferred cloud partner(s)
- Future growth potential ([Landed Addressable Market (LAM)](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam)), and 
- Preference for [SaaS or Self-Managed](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/)

# Understand Value Points
The second phase of discovery uses an iterative insight/question loop, often across multiple meetings or conversations, to uncover the prospect’s underlying challenges and motivations, with the ultimate goal of constructing a highly relevant and differentiated value message that ties the GitLab solution to the prospect’s business needs.

## Preparation
Ensure you are ready for each discussion. The level of preparation for each discovery conversation will vary, so use your best judgment for how deep you go and be sure to leverage the information that has already been collected during Prospecting and Qualification. The key point is to be prepared and not wing it. Below are recommended considerations.
1. **Research the prospect** - This may include but is not limited to:
    - Annual reports and/or financial statements
    - Account activity/engagement history in Salesforce
    - Recent news or press releases
1. **Understand the persona** - Different toles within the prospect's organization will inherently care about and be motivated by different things. Consider the following:
    - Understand the persona you are engaging 
        - [Buyer personas](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)
        - [User personas](/handbook/marketing/strategic-marketing/roles-personas/#user-personas)
    - Research your contact(s) on LinkedIn, Twitter, and or other social media platforms
1. **Develop a [Pre-Call Plan](https://docs.google.com/document/d/1yjyfvMoDvayZca5hXiIwSHYc9T1M3mTc7ocqzjhqOf8/edit?usp=sharing)** - As outlined in the job aid, be sure to consider the following:
    - What is your desired outcome for this conversation or meeting? What do you think the prospect's desired outcome is?
    - What information do you need to gather to achieve your objective(s)? What discovery questions will you ask, how, and in what sequence? As outlined above, this iterative insight/question loop will often span across multiple meetings or conversations.
        - Start with [Customer Strategy-based discovery questions](/handbook/sales/qualification-questions/#customer-strategy) to seek to understand your prospect's goals, objectives, and initiatives
        - Next, ask open-ended [Customer Needs-based discovery questions](/handbook/sales/qualification-questions/#customer-needs) in pursuit of understanding your prospect's desired outcomes, current situation and environment and level of satisfaction, and motivations (including personal needs)
        - As you develop and prioritize your discovery questions, consider what types of questions you can ask that reinforce [GitLab's differentiators](/handbook/sales/command-of-the-message/#gitlab-differentiators)
            - For a deeper dive and sample trap-setting discovery questions (trapping the competition, NOT the customer), re-review the Defensible Differentiators content in the GitLab Value Framework doc (see top asset highlighted in the [Resources: Core Content](/handbook/sales/command-of-the-message/#resources-core-content) section of the Command of the Message Handbook page)

## Key Tips
1. Approach discovery conversations with a curiosity-led mindset and a desire to better understand the customer’s challenge(s) as well as the implications of those challenges
    - How big is the problem?
    - How does this impact higher-level goals and objectives?
    - How does this challenge impact the person(s) with whom you are talking?
    - Who else in the organization does this challenge impact and how?
1. Leverage the TED questioning model, e.g.:
    - T: **Tell** me more about...
    - E: Can you please **explain** what that is?
    - D: Will you please *describe** how it happened?
1. Use the [Five Whys](/handbook/customer-success/tam/success-plans/questions-techniques/#five-whys) technique to identify the root cause(s) and understand the "So what?"
    - [Why Salespeople Should Ask The Same Question 5 Times In A Row](https://blog.hubspot.com/sales/the-five-whys-sales-strategy) (blog)
    - [The 'Five Whys Technique' to uncover your customer's pain points](https://youtu.be/c2jSn5AdBYY) (video, 1.5 minutes)
1. Commit to [active and effective listening](/handbook/communication/#effective-listening)
    - [Improve Your Active Listening Skills](https://podcasts.apple.com/us/podcast/improve-your-active-listening-skills-w-patrick-mcloughlin/id991362894?i=1000498972259) (podcast, 15 minutes)
1. Master [Command of the Message](/handbook/sales/command-of-the-message)
    - [Command of the Message e-learning module](https://gitlab.edcast.com/insights/ECL-20f0f2ac-0d50-4384-b473-63cc6d3bb48d) (90 minutes) (GitLab internal with protected IP from Force Management)

## Pitfalls to Avoid
Other than inadequate preparation, below are common discovery pitfalls you want to be sure to avoid. 
1. **Happy ears** - Resist the urge to prematurely talk about or begin to position GitLab until you have a thorough understanding of the customer’s challenge(s) as well as the implications of those challenges and if and how they attach to higher-level initiatives, objectives, or goals.
1. **Not attaching to the biggest problem** 
1. **Failing to identify the negative consequences of doing nothing**
1. **Lack of multi-threading**

# Identify the Buying Process
The final phase of discovery identifies the roadmap, along with potential roadblocks, for getting a deal closed as well as key stakeholders who must be involved.
1. Leverage [Customer Decision-based discovery questions](https://about.gitlab.com/handbook/sales/qualification-questions/#customer-decision)
1. Begin thinking about [MEDDPPICC](/handbook/sales/meddppicc/)
    - [MEDDPPICC e-learning module](https://gitlab.edcast.com/insights/ECL-315a08a3-c8e2-48e1-9d4e-b43a6afd440b) (60 minutes) (GitLab internal)
