---
layout: handbook-page-toc
title: "Enterprise Sales Playbook"
description: "The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# Overview 
The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance. Field Enablement also plans to launch a Commercial Sales Playbook as part of the [Commercial Sales Handbook](/handbook/sales/commercial/) (timing to be determined by the COM Sales Enablement Manager inn collaboration with COM Sales leadership).

## Educate & Engage

### Prospecting

## Facilitate the Opportunity

### [Discovery](/handbook/sales/playbook/discovery)

### Demo

### Proof of Value

### Making the Business Case

### Champion Development

## Deal Closure

### Mutual Close Plan

### Create a Customer-Centric Proposal

### Quoting

### Negotiate

## Retain and Expand

### Customer Onboarding

### Success Planning

### EBRs
