---
layout: blank
title: "Take GitLab's 2021 DevSecOps survey"
description: "Take the survey. Get free swag. It's that easy!"
canonical_path: "/2021-survey/"
suppress_header: true
twitter_image: '/images/opengraph/2021-dev-survey-1-shirt.png'
---

<iframe src="https://gitlab.fra1.qualtrics.com/jfe/form/SV_eCFcNsPO8fuGzbw"></iframe>
<style type="text/css">
  iframe {
    height: 100vh;
    width: 100vw;
  }
</style>
