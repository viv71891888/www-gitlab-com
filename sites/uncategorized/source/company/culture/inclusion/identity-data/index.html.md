---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-04-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 148       | 11.44%          |
| Based in EMEA                               | 364       | 28.13%          |
| Based in LATAM                              | 23        | 1.78%           |
| Based in NORAM                              | 759       | 58.66%          |
| **Total Team Members**                      | **1,294** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 879       | 67.93%          |
| Women                                       | 415       | 32.07%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,294** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 109       | 61.58%          |
| Women in Management                         | 68        | 38.42%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **177**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 76        | 71.70%          |
| Women in Leadership                         | 30        | 28.30%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **106**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 440       | 79.71%          |
| Women in Engineering                        | 112       | 20.29%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **552**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 66        | 9.35%           |
| Black or African American                   | 22        | 3.12%           |
| Hispanic or Latino                          | 39        | 5.52%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 33        | 4.67%           |
| White                                       | 537       | 76.06%          |
| Unreported                                  | 8         | 1.13%           |
| **Total Team Members**                      | **706**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 21        | 10.66%          |
| Black or African American                   | 3         | 1.52%           |
| Hispanic or Latino                          | 6         | 3.05%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 10        | 5.08%           |
| White                                       | 156       | 79.19%          |
| Unreported                                  | 1         | 0.51%           |
| **Total Team Members**                      | **197**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 8.62%           |
| Black or African American                   | 4         | 3.45%           |
| Hispanic or Latino                          | 4         | 3.45%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 6.03%           |
| White                                       | 91        | 78.45%          |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **116**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 14        | 16.67%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.57%           |
| White                                       | 67        | 79.76%          |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **84**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 151       | 11.67%          |
| Black or African American                   | 35        | 2.70%           |
| Hispanic or Latino                          | 71        | 5.49%           |
| Native Hawaiian or Other Pacific Islander   | 2         | 0.15%           |
| Two or More Races                           | 42        | 3.25%           |
| White                                       | 827       | 63.91%          |
| Unreported                                  | 165       | 12.75%          |
| **Total Team Members**                      | **1,294** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 68        | 12.32%          |
| Black or African American                   | 11        | 1.99%           |
| Hispanic or Latino                          | 28        | 5.07%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 16        | 2.90%           |
| White                                       | 335       | 60.69%          |
| Unreported                                  | 92        | 16.85%          |
| **Total Team Members**                      | **552**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 9.04%           |
| Black or African American                   | 4         | 2.26%           |
| Hispanic or Latino                          | 5         | 2.82%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 4.52%           |
| White                                       | 129       | 72.88%          |
| Unreported                                  | 15        | 8.47%           |
| **Total Team Members**                      | **177**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 15        | 14.15%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.94%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.83%           |
| White                                       | 78        | 73.58%          |
| Unreported                                  | 9         | 8.49%           |
| **Total Team Members**                      | **106**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 16        | 1.24%           |
| 25-29                                       | 193       | 14.91%          |
| 30-34                                       | 358       | 27.67%          |
| 35-39                                       | 310       | 23.96%          |
| 40-49                                       | 280       | 21.64%          |
| 50-59                                       | 117       | 9.04%           |
| 60+                                         | 20        | 1.55%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,294** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
