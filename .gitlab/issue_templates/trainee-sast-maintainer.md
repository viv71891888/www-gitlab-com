<!--

Before nominating yourself as a trainee, talk with your manager and team about it, as the traineeship is likely to impact your capacity.

-->
## Basic setup

1. [ ] Change this issue title to include your name, project, and maintainer type:
    `Trainee SAST Maintainer: [full name] [project type]`.
1. [ ] Indicate your selected analyzer projects (limit to 1 subgroup per trainee issue):
    1. Analyzer projects
        1. [ ] `analyzers/bandit`
        1. [ ] `analyzers/brakeman`
        1. [ ] `analyzers/eslint`
        1. [ ] `analyzers/flawfinder`
        1. [ ] `analyzers/gosec`
        1. [ ] `analyzers/kubesec`
        1. [ ] `analyzers/mobsf`
        1. [ ] `analyzers/nodejs-scan`
        1. [ ] `analyzers/phpcs-security-audit`
        1. [ ] `analyzers/pmd-apex`
        1. [ ] `analyzers/secrets`
        1. [ ] `analyzers/security-code-scan`
        1. [ ] `analyzers/semgrep`
        1. [ ] `analyzers/sobelow`
        1. [ ] `analyzers/spotbugs`
    1. Shared common projects
        1. [ ] `analyzers/command`
        1. [ ] `analyzers/report`
        1. [ ] `analyzers/ruleset`
    1. Post-Analyzer projects
        1. [ ] `post-analyzers/tracking-calculator`
1. [ ] Read the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/)
1. [ ] Understand [how to become a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer) 
1. [ ] Understand our [Secure Team standards and style guidelines](https://docs.gitlab.com/ee/development/go_guide/#secure-team-standards-and-style-guidelines)
1. [ ] Understand our [Secure Release Process](https://about.gitlab.com/handbook/engineering/development/secure/release_process.html)
1. [ ] Understand our [Secure QA Process](https://about.gitlab.com/handbook/engineering/development/secure/qa_process.html)
1. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md))
       adding yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer)
1. [ ] Ask your manager to set up a check in on this issue every six weeks or so.

## Working towards becoming a maintainer

There is no checklist here, only guidelines. Remember that there is no specific timeline on this.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.

After each MR is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback, and compare
this review to the average maintainer review.
```

**Tip:** There are [tools](https://about.gitlab.com/handbook/tools-and-tips/#trainee-maintainer-issue-upkeep) available to assist with this task.

## When you're ready to make it official

When reviews have accumulated, and recent reviews consistently fulfill
maintainer responsibilities, any maintainer can take the next step. The trainee
should also feel free to discuss their progress with their manager or any
maintainer at any time.

1. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md) proposing yourself as a maintainer.
2. [ ] Create a merge request for `CODEOWNERS` for the relevant project, adding yourself accordingly, and ask a maintainer to review it.
3. [ ] Keep reviewing, start merging :metal:
3. [ ] Keep reviewing, and helping with merge requests! :tada:
4. [ ] **Important Read**: If you are not currently a backend or frontend maintainer, please assign the merge requests to a maintainer who can merge on your behalf, specifying that it has already been approved by a CI/CD templates maintainer.


/label ~"trainee maintainer" ~"group::static analysis"
